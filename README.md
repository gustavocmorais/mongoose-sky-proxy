# A MQTT/HTTP Proxy Mongoose OS app

## Overview

This project is a proof of concept on how to integrate a Directv/Sky decoder with AWS IOT Cloud.
It is able to receive commands via MQTT, call it via local HTTP and post back the response on MQTT.

It also is capable of searching devices in the local network, using UPNP, and posting them in the cloud.

## How to install this app

- Install and start [mos tool](https://mongoose-os.com/software.html)
- Switch to the Project page, find and import this app, build and flash it:

<p align="center">
  <img src="https://mongoose-os.com/images/app1.gif" width="75%">
</p>
