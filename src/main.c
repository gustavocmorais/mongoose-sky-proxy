#include <stdio.h>
#include "mgos.h"
#include "mgos_mqtt.h"
#include "common/json_utils.h"
#include "mongoose.h"

struct mg_connection *udp = NULL;

static void decoder_to_mqtt(char *decoder) {
  char topic[100], message[256];
  struct json_out out = JSON_OUT_BUF(message, sizeof(message));
   
  time_t now=time(0);
  struct tm *timeinfo = localtime(&now);
 
  snprintf(topic, sizeof(topic), "/things/%s/decoder",mgos_sys_config_get_device_id());
  json_printf(&out, "{total_ram: %lu, free_ram: %lu, decoder: \"%s\", device: \"%s\", timestamp: \"%02d:%02d:%02d\"}",
              (unsigned long) mgos_get_heap_size(),
              (unsigned long) mgos_get_free_heap_size(),
              decoder,
              (char *) mgos_sys_config_get_device_id(),
              (int) timeinfo->tm_hour,
              (int) timeinfo->tm_min,
              (int) timeinfo->tm_sec);
  bool res = mgos_mqtt_pub(topic, message, strlen(message), 1, false);
  LOG(LL_INFO, ("Published to MQTT: %s", res ? "yes" : "no"));

}

static void validate_decoder(char *buf, int len) {

  if(strstr(buf, "DIRECTV") != NULL) {
    LOG(LL_INFO, ("Achei decoder!!!"));

    char * curLine = buf;
  
    while(curLine) {
      char * nextLine = strchr(curLine, '\n');
      if (nextLine) *nextLine = '\0';  // temporarily terminate the current line

      if(strstr(curLine, "Location: ") != NULL) {
        LOG(LL_INFO, ("Achei location!!!"));
        decoder_to_mqtt(curLine);
      }

      //LOG(LL_INFO,("curLine=[%s]\n", curLine));
      if (nextLine) *nextLine = '\n';  // then restore newline-char, just to be tidy    
      curLine = nextLine ? (nextLine+1) : NULL;
    }
    
  }

  (void) buf;
  (void) len;
}

static void ev_multicast_handler(struct mg_connection *nc, int ev, void *p,
                       void *user_data) {
  
  //LOG(LL_INFO, ("ev_multicast_handler: %d", ev));
  
  struct mbuf *io = &nc->recv_mbuf;
  (void) p;
  switch (ev) {

    case MG_EV_RECV:
      //LOG(LL_INFO, ("Received (%zu bytes): '%.*s'\n", io->len, (int) io->len, io->buf));
      validate_decoder(io->buf, io->len);
      //mbuf_remove(io, io->len);
      //nc->flags |= MG_F_SEND_AND_CLOSE;
      break;
    default:
      break;
  }

  (void) user_data;
}


static void find_decoders () {

  udp = mg_connect(mgos_get_mgr(), 
    "udp://239.255.255.250:1900", ev_multicast_handler, NULL);

    mg_printf(udp,
              "M-SEARCH * HTTP/1.1\r\n"
              "HOST:239.255.255.250:1900\r\n"
              //"ST:ssdp:all\r\n"
              "ST: urn:schemas-upnp-org:device:MediaRenderer:1\r\n"
              "MAN:\"ssdp:discover\"\r\n"
              "MX:3\r\n\r\n");
  
}

static void net_cb(int ev, void *evd, void *arg) {
  switch (ev) {
    case MGOS_NET_EV_DISCONNECTED:
      LOG(LL_INFO, ("%s", "Net disconnected"));
      break;
    case MGOS_NET_EV_CONNECTING:
      LOG(LL_INFO, ("%s", "Net connecting..."));
      break;
    case MGOS_NET_EV_CONNECTED:
      LOG(LL_INFO, ("%s", "Net connected"));
      break;
    case MGOS_NET_EV_IP_ACQUIRED:
      LOG(LL_INFO, ("%s", "Net got IP address"));
      break;
  }
 
  (void) evd;
  (void) arg;
}
 

  
static void button_cb(int pin, void *arg) {

  LOG(LL_INFO, ("Button presses on pin: %d", pin));
  find_decoders(NULL);
  (void) arg;
}


static void response_to_mqtt(char *response) {
  char topic[100], message[256];
  struct json_out out = JSON_OUT_BUF(message, sizeof(message));
   
  time_t now=time(0);
  struct tm *timeinfo = localtime(&now);
 
  snprintf(topic, sizeof(topic), "/things/%s/response",mgos_sys_config_get_device_id());
  json_printf(&out, "{total_ram: %lu, free_ram: %lu, response: \"%s\", device: \"%s\", timestamp: \"%02d:%02d:%02d\"}",
              (unsigned long) mgos_get_heap_size(),
              (unsigned long) mgos_get_free_heap_size(),
              response,
              (char *) mgos_sys_config_get_device_id(),
              (int) timeinfo->tm_hour,
              (int) timeinfo->tm_min,
              (int) timeinfo->tm_sec);
  bool res = mgos_mqtt_pub(topic, message, strlen(message), 1, false);
  LOG(LL_INFO, ("Published to MQTT: %s", res ? "yes" : "no"));

}

 

static void sub(struct mg_connection *c, const char *fmt, ...) {
  char buf[100];
  struct mg_mqtt_topic_expression te = {.topic = buf, .qos = 1};
  uint16_t sub_id = mgos_mqtt_get_packet_id();
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buf, sizeof(buf), fmt, ap);
  va_end(ap);
  mg_mqtt_subscribe(c, &te, 1, sub_id);
  LOG(LL_INFO, ("Subscribing to %s (id %u)", buf, sub_id));
}

static void ev_http_handler(struct mg_connection *nc, int ev, void *p,
                       void *user_data) {
  struct http_message *hm = (struct http_message *) p;
  int connect_status;

  //LOG(LL_INFO, ("evento http: %d", ev));

  switch (ev) {
    
    case MG_EV_CONNECT:
      connect_status = *(int *) p;
      LOG(LL_INFO, ("Server connected: %d\n", connect_status));
      if (connect_status != 0) {
        LOG(LL_INFO, ("Error connecting: %s\n", strerror(connect_status)));
        //s_exit_flag = 1;
      }
      break;
    case MG_EV_HTTP_REPLY:
      LOG(LL_INFO, ("Got reply:\n%.*s\n", (int) hm->body.len, hm->body.p));
      nc->flags |= MG_F_SEND_AND_CLOSE;
      char buf[512];
      sprintf(buf, "%.*s", (int) hm->body.len, hm->body.p);
      response_to_mqtt(buf);
      //s_exit_flag = 1;
      break;
    case MG_EV_CLOSE:
      //if (s_exit_flag == 0) {
        LOG(LL_INFO, ("Server closed connection\n"));
        //s_exit_flag = 1;
       // mg_mgr_free(&mgr);
      //};
      break;
    default:
      break;
  }

  (void) user_data;
}


static void ev_handler(struct mg_connection *c, int ev, void *p,
                       void *user_data) {
  struct mg_mqtt_message *msg = (struct mg_mqtt_message *) p;

  if (ev == MG_EV_MQTT_CONNACK) {
    LOG(LL_INFO, ("CONNACK: %d", msg->connack_ret_code));
    sub(c, "/things/%s/action",mgos_sys_config_get_device_id());
    //report_to_mqtt();

  } 
  
  else if (ev == MG_EV_MQTT_SUBACK) {
    LOG(LL_INFO, ("Subscription %u acknowledged", msg->message_id));
  } 
  
  else if (ev == MG_EV_MQTT_PUBLISH) {
    struct mg_str *s = &msg->payload;

    LOG(LL_INFO, ("got command: [%.*s]", (int) s->len, s->p));
    /* Our subscription is at QoS 1, we must acknowledge messages sent ot us. */
    mg_mqtt_puback(c, msg->message_id);

    char *action = 0;
    
    //veio ACTION
    if (json_scanf(s->p, s->len, "{action: %Q}", &action) > 0 ) {
      LOG(LL_INFO, ("action: %s", action));

      //troca de estado do led
      if (!strcmp(action, "SHEF")) {
        char *command = 0;
        
        //device
        if (json_scanf(s->p, s->len, "{command: %Q}", &command) > 0) {
          LOG(LL_INFO, ("command: %s", command));
          
          //envia comando para o decoder 
          mg_connect_http(mgos_get_mgr(), ev_http_handler, NULL, command, NULL, NULL);
          
          free(command); 
              
        }

        else {
            LOG(LL_INFO, ("faltou o command!!!"));
        }

        


      }

      //reporta status
      else if (!strcmp(action, "REPORT")) {
        //report_to_mqtt();
      }

    }
    
    else {
      LOG(LL_INFO, ("error command: unknown"));
      
    }

  }
  (void) user_data;
}



enum mgos_app_init_result mgos_app_init(void) {

  
  mgos_set_timer(30*60*1000, MGOS_TIMER_REPEAT, find_decoders, NULL);
 
  /* Network connectivity events */
  mgos_event_add_group_handler(MGOS_EVENT_GRP_NET, net_cb, NULL);

  mgos_mqtt_add_global_handler(ev_handler, NULL);

  //reboot every 12h
  mgos_system_restart_after(43200*1000);

  mgos_gpio_set_button_handler(0,
                               MGOS_GPIO_PULL_UP, MGOS_GPIO_INT_EDGE_NEG, 200,
                               button_cb, NULL);


  return MGOS_APP_INIT_SUCCESS;
}



/*
chamada de teste:

topico:
/things/esp8266_1216CD/action

payload:
{
  "action": "SHEF",
  "command": "http://192.168.0.128:8080/tv/tune?major=405"
}

*/